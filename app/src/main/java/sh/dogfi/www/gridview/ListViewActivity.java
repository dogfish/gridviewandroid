package sh.dogfi.www.gridview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.EditText;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.content.ContentValues;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.view.View;
import java.util.ArrayList;
import sh.dogfi.www.gridview.db.TaskContract;
import sh.dogfi.www.gridview.db.TaskDbHelper;
import android.widget.Toast;
import android.widget.ImageView;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

public class ListViewActivity extends AppCompatActivity {
    private TaskDbHelper mHelper;
    private ListView mTaskListView;
    private ArrayAdapter<String> mAdapter;

    CarouselView carouselView;
    CarouselView customCarouselView;

    int[] sampleImages = {R.drawable.todo_3, R.drawable.todo_1, R.drawable.todo_5, R.drawable.todo_4, R.drawable.todo_2};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_view);
        carouselView = (CarouselView) findViewById(R.id.carouselView);
        carouselView.setPageCount(sampleImages.length);

        carouselView.setImageListener(imageListener);

        mHelper = new TaskDbHelper(this);
        mTaskListView = (ListView) findViewById(R.id.list_todo);

        updateUI();
    }

    ImageListener imageListener = new ImageListener() {
        @Override
        public void setImageForPosition(int position, ImageView imageView) {
            imageView.setImageResource(sampleImages[position]);
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_task:
                final EditText taskEditText = new EditText(this);
                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setTitle("Add a new task")
                        .setMessage("What do you want to do next?")
                        .setView(taskEditText)
                        .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                if(taskEditText.getText().toString().length()==0)
                                {
                                    Toast.makeText(ListViewActivity.this, "Task cannot be Blank",
                                            Toast.LENGTH_LONG).show();
                                }
                                else {
                                    SQLiteDatabase checkdb = mHelper.getReadableDatabase();
                                    String value = String.valueOf(taskEditText.getText());
                                    Cursor cursor=checkdb.rawQuery("SELECT "+TaskContract.TaskEntry.COL_TASK_TITLE+" FROM "+TaskContract.TaskEntry.TABLE+" WHERE "+TaskContract.TaskEntry.COL_TASK_TITLE+"='"+value+"'",null);
                                    if (cursor.moveToFirst())
                                    {
                                        checkdb.close();
                                        Toast.makeText(ListViewActivity.this, "Task Already Exits",
                                                Toast.LENGTH_LONG).show();

                                    }
                                    else {
                                        String task = String.valueOf(taskEditText.getText());
                                        SQLiteDatabase db = mHelper.getWritableDatabase();
                                        ContentValues values = new ContentValues();
                                        values.put(TaskContract.TaskEntry.COL_TASK_TITLE, task);
                                        db.insertWithOnConflict(TaskContract.TaskEntry.TABLE,
                                                null,
                                                values,
                                                SQLiteDatabase.CONFLICT_REPLACE);
                                        db.close();
                                        updateUI();
                                        Toast.makeText(ListViewActivity.this, "Task is created",
                                                Toast.LENGTH_SHORT).show();
                                    }

                                }

                        }
                        })
                        .setNegativeButton("Cancel", null)
                        .create();
                dialog.show();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void deleteTask(View view) {
        final View parent = (View) view.getParent();
        final TextView taskTextView = (TextView) parent.findViewById(R.id.task_title);
        final String task = String.valueOf(taskTextView.getText());
        AlertDialog dialog = new AlertDialog.Builder(this)
                .setTitle("Are you sure you are done with this task")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        SQLiteDatabase db = mHelper.getWritableDatabase();
                        db.delete(TaskContract.TaskEntry.TABLE,
                                TaskContract.TaskEntry.COL_TASK_TITLE + " = ?",
                                new String[]{task});
                        db.close();
                        updateUI();
                        Toast.makeText(ListViewActivity.this, "Deleted",
                                Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }

    private void updateUI() {
        ArrayList<String> taskList = new ArrayList<>();
        SQLiteDatabase db = mHelper.getReadableDatabase();
        Cursor cursor = db.query(TaskContract.TaskEntry.TABLE,
                new String[]{TaskContract.TaskEntry._ID, TaskContract.TaskEntry.COL_TASK_TITLE},
                null, null, null, null, null);
        while (cursor.moveToNext()) {
            int idx = cursor.getColumnIndex(TaskContract.TaskEntry.COL_TASK_TITLE);
            taskList.add(cursor.getString(idx));
        }

        if (mAdapter == null) {
            mAdapter = new ArrayAdapter<>(this,
                    R.layout.item_todo,
                    R.id.task_title,
                    taskList);
            mTaskListView.setAdapter(mAdapter);
        } else {
            mAdapter.clear();
            mAdapter.addAll(taskList);
            mAdapter.notifyDataSetChanged();
        }

        cursor.close();
        db.close();
    }
}





