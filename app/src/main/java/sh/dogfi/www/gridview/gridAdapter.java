package sh.dogfi.www.gridview;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.net.Uri;

import com.squareup.picasso.Picasso;

public class gridAdapter extends BaseAdapter {
    private Context mContext;

    // Keep all Images in array
    public Integer[] mThumbIds = {
            R.drawable.sample_10, R.drawable.sample_13,
            R.drawable.sample_26, R.drawable.sample_23,
            R.drawable.sample_2, R.drawable.sample_20,
            R.drawable.sample_18, R.drawable.sample_19,
            R.drawable.sample_15, R.drawable.sample_5,
            R.drawable.sample_24, R.drawable.sample_25,
            R.drawable.sample_6, R.drawable.sample_11,
            R.drawable.download, R.drawable.download1,
            R.drawable.download2, R.drawable.sample_0,
            R.drawable.sample_12, R.drawable.sample_1,
            R.drawable.sample_14, R.drawable.sample_3,
            R.drawable.sample_4, R.drawable.sample_15,
            R.drawable.sample_16, R.drawable.sample_17,
            R.drawable.sample_7, R.drawable.sample_21,
            R.drawable.sample_22

    };

    // Constructor
    public gridAdapter(Context c) {
        mContext = c;
    }

    @Override
    public int getCount() {
        return mThumbIds.length;
    }

    @Override
    public Object getItem(int position) {
        return mThumbIds[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView imageView = new ImageView(mContext);
        imageView.setImageResource(mThumbIds[position]);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setLayoutParams(new GridView.LayoutParams(250, 250));
        return imageView;
    }

}