package sh.dogfi.www.gridview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {
    Intent myIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void gridView(View view) {
        myIntent = new Intent(MainActivity.this,
                gridview.class);
        startActivity(myIntent);
    }

    public void listView(View view) {
        myIntent = new Intent(MainActivity.this,
                ListViewActivity.class);
        startActivity(myIntent);
    }
}
