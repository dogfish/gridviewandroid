# GridView,ListView & CarouselView In Android Native with Animations #

The ListView and GridView are subclasses of AdapterView and they can be populated by binding them to an Adapter, which retrieves data from an external source and creates a View that represents each data entry.


![Screenshot_2017-03-08-18-08-17.png](https://bitbucket.org/repo/7574jB/images/3205899679-Screenshot_2017-03-08-18-08-17.png)

![Screenshot_2017-03-01-19-57-32.png](https://bitbucket.org/repo/7574jB/images/1870002286-Screenshot_2017-03-01-19-57-32.png)


**Adapter:**

* An adapter actually bridges between UI components and the data source that fill data into UI Component. Adapter can be used to supply the data to like spinner, list view, grid view etc.
* An AdapterView is a view whose children are determined by an Adapter.

The ArrayAdapter class can handle a list or array of Java objects as input. Every Java object is mapped to one row. By default it maps the toString() method of the object to a view in the row layout. An adapter is used for managing the items in the list (the data model or data source).

**GridView:**

* Android’s GridView is a useful component for constructing easy-to-browse lists. And since grids are everywhere these days it’s a necessary skill.

GridView follows the formula to render each cell. Basically, you:

* Create a BaseAdapter subclass.
* Set an instance of this class as your data provider to the GridView.
* Return each cell’s view from getView() on your adapter.

And here's some code! :thumbsup:

```
#!java

GridView gridview = (GridView) findViewById(R.id.gridview);
gridview.setAdapter(new gridAdapter(this)); 
```

ListView follows the same formula to render each rows.

**CarouselView:**

To implement the carouselView in the project,I have used third party Library.

To import Library in Android Studio add dependencies in build.gradle file as:

```
#!java

compile 'com.synnapps:carouselview:0.0.10'
```
**To access the Camera and Gallery of device set the permissions in AndroidManifest file :
**
```
#!xml

<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.CAMERA" />
<uses-feature android:name="android.hardware.camera" />
<uses-feature android:name="android.hardware.camera.autofocus" />
```
 
### How it works ###

ListView and GridView is designed for scalability and performance. In practice, this essentially means:

It tries to do as few view inflations as possible.
It only paints and lays out children that are (or are about to become) visible on screencode.

### Stuff used to make this: ###

[User Interface](https://developer.android.com/guide/topics/ui/declaring-layout.html) Android Developer

[Carousel View](https://github.com/sayyam/carouselview) Library

[Performance Tips](http://lucasr.org/2012/04/05/performance-tips-for-androids-listview/) for ListView